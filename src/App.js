import { Component } from 'react';
import axios from 'axios';
import './App.css';
import Background from './flower.png';

class App extends Component {
  state = {
    data: [],
    title: '',
    content: '',
    titleUpdate: '',
    contentUpdate: ''
  }

  handleChangeTitle = (event) => {
    this.setState({title: event.target.value});
  }

  handleChangeContent = (event) => {
    this.setState({content: event.target.value});
  }

  handleChangeTitleUpdate = (event) => {
    this.setState({titleUpdate: event.target.value});
  }

  handleChangeContentUpdate = (event) => {
    this.setState({contentUpdate: event.target.value});
  }

  handleSubmit = (event) => {
    event.preventDefault()

    axios.post('https://sistech-api.vercel.app/blog/', {
      title: this.state.title,
      content: this.state.content
    }, {
      headers: {
        'Authorization': 'Bearer ' + '8a45c123-9593-489c-8ce2-b9c08c28db60'
      }
    })
    .then((response) => {
      // handle success
      alert(`Berhasil membuat post baru!`)
      window.location.reload()
    })
  }

  handleUpdate = (event, id) => {
    event.preventDefault()

    axios.put('https://sistech-api.vercel.app/blog/', {
      title: this.state.titleUpdate,
      content: this.state.contentUpdate,
      id: id
    }, {
      headers: {
        'Authorization': 'Bearer ' + '8a45c123-9593-489c-8ce2-b9c08c28db60'
      }
    })
    .then(() => {
      // handle success
      alert(`Post berhasil terupdate!`)
      window.location.reload()
    })
  }

  toggleEdit = (event, id) => {
    event.preventDefault()

    let posts = this.state.data
    let post = posts.find(p => p.id === id);
    post.edit = !post.edit

    this.setState({
      data: posts
    })
  }

  handleLike = (event, id) => {
    event.preventDefault()

    axios.post('https://sistech-api.vercel.app/blog/like', {
      id: id,
    }, {
      headers: {
        'Authorization': 'Bearer ' + '8a45c123-9593-489c-8ce2-b9c08c28db60'
      }
    })
    .then(() => {
      // handle success
      alert(`Berhasil menyukai post!`)
      window.location.reload()
    })
  }

  componentDidMount() {
    axios.get('https://sistech-api.vercel.app/blog/', {
      headers: {
        'Authorization': 'Bearer ' + '8a45c123-9593-489c-8ce2-b9c08c28db60'
      }
    })
    .then((response) => {
      let data = response.data
      data.map(d => d.edit = false)

      this.setState({
        data: data
      })
    })
  }

  render() {
    return (
      <div className="App" >
          <h1 style={{color: "#E41B17"}}>About Shirin ✨ </h1>
          <div style={{border: "3px solid #E41B17",margin: "0 auto 1em", maxWidth: "640px"}}>
            <h3 style={{color: "#E41B17"}}>Biodata 👧🏻</h3>
            <p>
            Name : Shirin Zarqaa Rabbaanii Arham <br />
            DoB: Jakarta, 3 Mei 2004 <br />
            Address : Komplek Telkom Satwika Permai Blok B1 No. 2 <br />
            Contact : 081283776985 <br />
            E-mail : Shirinzarqaa354@gmail.com <br />
            </p>
            <h3 style={{color: "#E41B17"}}>Achievement 🌟</h3>
            <p>
            Mahasiswa Baru Ilmu Komputer Universitas Indonesia 2022 <br />
            Juara 1 Lomba Panahan 17-an 2019 Kompleks Satwika Permai<br />
            </p>
            <h3 style={{color: "#E41B17"}}>Experience 💼</h3>
            <p>
            Anggota organisasi GDS SMAN 9 Jakarta<br />
            Bendahara + wakil ketua kelas pada saat SMAN 9 Jakarta <br />
            </p>
          </div>
          <br /><br />
          <h1 style={{color: "#E41B17"}}>Shirin's Blog ✍🏻</h1>
          {
            this.state.data.map(post => <div style={{
                border: "2px solid pink", 
                borderTop: "0", 
                padding: "2em",
                margin: "0 auto 1em",
                backgroundImage: `url(${Background})`,
                maxWidth: "640px"
              }}>
              <h1 style={{color: "#E41B17"}}>{post.title}</h1>
              <p>{post.content}</p>
              <p>{post.like} 👍</p>
              <button onClick={(e) => this.handleLike(e, post.id)} style={{padding: "1em", marginRight: "1em", border: "0", backgroundColor: "pink"}}>Like</button>
              <button onClick={(e) => this.toggleEdit(e, post.id)} style={{padding: "1em", border: "0", backgroundColor: "pink"}}>Edit</button>

              {
                post.edit ? <div>
                  <form onSubmit={(e) => this.handleUpdate(e, post.id)}>
                    <label>
                      <p style={{marginBottom: "0"}}>Title:</p>
                      <input size="60" type="text" name="title" placeholder={post.title} value={this.state.titleUpdate} onChange={this.handleChangeTitleUpdate} style={{margin: "1em"}}/>
                    </label>
                    <br />
                    <label>
                      <p style={{marginBottom: "0"}}>Content:</p>
                      <textarea  cols="40" rows="5" name="content" placeholder={post.content} value={this.state.contentUpdate} onChange={this.handleChangeContentUpdate} style={{margin: "1em"}}></textarea>
                    </label>
                    <br />
                    <input  style={{padding: "1em", border: "0", backgroundColor: "pink"}} type="submit" value="Update" />
                  </form>
                </div> : null
              }
              </div>)
          }

          <br/>
          <br/>

          <h2 style={{color: "#E41B17"}}>New Post</h2>
          <form onSubmit={this.handleSubmit} style={{
              border: "2px solid #E41B17", 
              borderTop: "0", 
              padding: "2em",
              margin: "0 auto 1em", 
              maxWidth: "640px"
            }}>
            <label>
              <p style={{marginBottom: "0"}}>Title:</p>
              <input size="60" type="text" name="title" value={this.state.title} onChange={this.handleChangeTitle} style={{margin: "1em"}}/>
            </label>
            <br />
            <label>
              <p style={{marginBottom: "0"}}>Content:</p>
              <textarea  cols="40" rows="5" type="text" name="content" value={this.state.content} onChange={this.handleChangeContent} style={{margin: "1em"}}></textarea>
            </label>
            <br />
            <input  style={{padding: "1em", border: "0", backgroundColor: "pink"}} type="submit" value="Create" />
          </form>
      </div>
    );
  }
}

export default App;
